% =========================================================================
% *** FUNCTION main
% =========================================================================
function main()

  global Nx
  global f
  global a


  Nx = 100;
  a0 = [0.2;-0.4];

  a = 1e2*a0;

  f = 1;

 compareSolversN( a, f )

  [A,b] = getPoissonConvection2D( Nx, a, f );

   % -----------------------------------------------------------------------
   % plot the solution
   figure;
   plotPoisson2D( A\b, 'naked', 1 );
   % -----------------------------------------------------------------------


  % set method parameters
  tol   = 1e-10;
  maxit = Nx*Nx; % check also higher values for maxit 2*Nx*Nx
  u0    = zeros(Nx*Nx,1);
  M1    = [];
  M2    = [];

%    % -----------------------------------------------------------------------
%    % CG method
%    [u,flag,relres,iter,resvec] = cg(A,b,tol,maxit,u0);
%    iterSteps = 1;
%    % -----------------------------------------------------------------------

%    % -----------------------------------------------------------------------
%    % CG method: pcg code
%    [u,flag,relres,iter,resvec] = pcg(A,b,tol,maxit);
%    iterSteps = 1;
%    iter = length(resvec)-1; % iter cgs=0, flag=1, no convergence in 
%                             % maxit iterations
%    % -----------------------------------------------------------------------

%    % -----------------------------------------------------------------------
%    % BiCG method
%    [u,flag,relres,iter,resvec] = bicg(A,b,tol,maxit,M1,M2,u0);
%    iterSteps = 1;
%    % -----------------------------------------------------------------------

%    % -----------------------------------------------------------------------
%    % BiCGStab method
%    [u,flag,relres,iter,resvec] = bicgstab(A,b,tol,maxit,M1,M2,u0);
%    iterSteps = 0.5;
%    % -----------------------------------------------------------------------


%    % -----------------------------------------------------------------------
%    % CGS method
%    [u,flag,relres,iter,resvec] = cgs(A,b,tol,maxit,M1,M2,u0);
%    iterSteps = 1;
%    iter = length(resvec)-1; % iter cgs=0, flag=1, no convergence in 
%                             % maxit iterations
%    % -----------------------------------------------------------------------

   % -----------------------------------------------------------------------
   % GMRES method
   restart = 30;
   [u,flag,relres,iter,resvec] = gmres(A,b,restart,tol,maxit,M1,M2,u0);
   iterSteps = 1;
   tmp = iter;
   iter = [];
   iter = (tmp(1)-1)*restart + tmp(2);
   % ----------------------------------------------------------------------

  % ----------------------------------------------------------------------
  % plot the residual
  figure;
  maxItems = 300; % maximum number of items to plot
  stepsize = ceil( length(resvec)/maxItems );
  semilogy( [0:iterSteps*stepsize:iter], resvec(1:stepsize:end) , 'r+' );
  set( gca, 'XLim', [0,iter] );
%    set( gca, 'XTick', [0:50:1000] );
%    set( gca, 'YTick', 10.^[-16:2:2] );
  % ----------------------------------------------------------------------


  % ----------------------------------------------------------------------
  % plot the relative residual
  figure;
  maxItems = 300; % maximum number of items to plot
  stepsize = ceil( length(resvec)/maxItems );
  relresvec = resvec / norm(b,2);
  semilogy( [0:iterSteps*stepsize:iter], relresvec(1:stepsize:end) , 'r+' );
  set( gca, 'XLim', [0,iter] );
 % set( gca, 'XTick', [0:50:1000] );
 % set( gca, 'YTick', 10.^[-16:2:2] );
  % ----------------------------------------------------------------------

end
% =========================================================================
% *** END FUNCTION main
% =========================================================================



% =========================================================================
% *** FUNCTION compareSolversN
% ***
% *** Plots the execution time TEPS and KEPS for different methods over the
% *** problem size parameter Nx.
% ***
% =========================================================================
function compareSolversN( a, f )

  tol   = 1e-10;
  maxit = 1000;

  minN  = 10;
  skipN = 10;
  maxN  = 200;

  n = ceil( (maxN - minN)/skipN );

  % make sure they all have the same calling API
  methods = { @cg , @bicg , @bicgstab , @cgs , @gmres30    };
  labels  = { 'CG', 'BiCG', 'BiCGStab', 'CGS', 'GMRES(30)' };

  NumMethods = length(methods);

  % initialize the stats vectors
  NN    = cell(NumMethods,1);
  time  = cell(NumMethods,1);
  iter  = cell(NumMethods,1);
  label = cell(NumMethods,1);

  k = 0;
  for  i = minN:skipN:maxN

      k = k+1;

      disp( sprintf( 'Calculating N=%d...\n', i ));

      % get full matrix
      Nx = i;
      [A,b] = getPoissonConvection2D( Nx, a, f );

      N = length( b );

      % -------------------------------------------------------------------
      % loop over the methods
      for l=1:NumMethods
          tic
          [u,flag,relres,it,resvec] = methods{l}(A,b,tol,maxit);
          t = toc;
          if ~flag
              NN{l}   = [NN{l}, i ];
              time{l} = [time{l}, t];
              iter{l} = [iter{l}, it];
          else
              flag
          end
      end
      % -------------------------------------------------------------------

  end

  markercolors = 'rgbkm';
  markershapes = '+ox*^';

  % -----------------------------------------------------------------------
  % plot the iterations
  plotLegend = [];
  figure;
  hold on;
  for l=1:NumMethods
      if isempty(iter{l})
          continue
      end
      plotLegend = [plotLegend, l];
      coloridx = mod(l-1,length(markercolors))+1;
      shapeidx = mod(l-1,length(markershapes))+1;
      style = [ markercolors(coloridx), markershapes(shapeidx) ];
      plot( NN{l}, iter{l}, style );
  end
  hold off;

  legend( {labels{plotLegend}}, 'Location', 'NorthWest' );
  xlabel('N')
  ylabel('k_{\varepsilon}')
  %matlab2tikz( 'comparison-iterations-convlarge.tikz','height', '3cm', 'width', '52mm' );
  % -----------------------------------------------------------------------


  % -----------------------------------------------------------------------
  % plot the computation times
  plotLegend = [];
  figure;
  hold on;
  for l=1:NumMethods
      if isempty(time{l})
          continue
      end
      plotLegend = [plotLegend, l];
      coloridx = mod(l-1,length(markercolors))+1;
      shapeidx = mod(l-1,length(markershapes))+1;
      style = [ markercolors(coloridx), markershapes(shapeidx) ];
      plot( NN{l}, time{l}, style );
  end
  hold off;

  legend( {labels{plotLegend}}, 'Location', 'NorthWest' );
  xlabel('N')
  ylabel('t_{\varepsilon}')
 % matlab2tikz( 'comparison-time-convlarge.tikz','height', '3cm', 'width', '52mm' );
  % -----------------------------------------------------------------------

  % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
  % wrapper for gmres to provide the same API as cg, bicg,...
  function [u,flag,relres,it,resvec] = gmres30(A,b,tol,maxit)
      restart = 30;
      [u,flag,relres,it,resvec] = gmres(A,b,restart,tol,maxit);
      it = restart*it(1) + it(2);
  end
  % = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

end
% =========================================================================
% *** END FUNCTION compareSolversN
% =========================================================================
