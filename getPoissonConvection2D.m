% =======================================================
% *** FUNCTION getPoissonConvection2D
% =======================================================
function [A,b] = getPoissonConvection2D( N, a, f )

  h = 1/(N+1);

  e = ones(N,1);
  I = speye(N);
  D = spdiags( [-e 2*e -e], [-1:1], N, N ) / h^2;

  CX = a(1) * spdiags( [-e e], [-1,1], N, N ) / (2*h);
  CY = a(2) * spdiags( [-e e], [-1,1], N, N ) / (2*h);

  A = kron(I,D+CX) + kron(D+CY,I);

  b = f * ones(N*N,1);

end
% =======================================================
% *** END FUNCTION getPoissonConvection2D
% =======================================================